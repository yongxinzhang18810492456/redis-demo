package com.xdclass.mobile.xdclassmobileredis;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class XdclassMobileRedisApplicationTests {
	@Autowired
	private RedisTemplate redisTemplate;
	private JedisPool pool = null;
	@Before
	public  void befaore() {
		if (pool == null) {
			JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
			jedisPoolConfig.setMaxTotal(10);
			pool = new JedisPool(jedisPoolConfig, "127.0.0.1", 6379, 10000);
		}
	}
	@Test
	public void contextLoads() {
		ValueOperations<String, List<String>> ops = redisTemplate.opsForValue();
		List<String> list = new ArrayList<>();
		String key = "list";
		list.add("1");
		list.add("2");
		ops.set(key,list);
		Jedis jedis= pool.getResource();
		Long incr = jedis.incr(key);
		ops.get(key);
		Long incr1 = jedis.incr(key);
		System.out.println(incr = incr1);
	}

}

