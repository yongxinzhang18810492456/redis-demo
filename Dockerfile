#FROM maven:3.6.3-jdk-11-slim AS MAVEN_BUILD
#FOR JAVA 8
FROM maven:3.5.2-jdk-8-alpine AS MAVEN_BUILD
ARG SPRING_ACTIVE_PROFILE
MAINTAINER Jasmin
COPY pom.xml /build/
COPY src /build/src/
WORKDIR /build/

RUN mvn clean install -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE -Dmaven.test.skip=true && mvn package -B -e -Dspring.profiles.active=$SPRING_ACTIVE_PROFILE -Dmaven.test.skip=true
#FROM openjdk:11-slim
# FOR JAVA 8
FROM openjdk:8-alpine
WORKDIR /app
COPY --from=MAVEN_BUILD /build/target/*.jar /app/xdclass-mobile-redis-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "xdclass-mobile-redis-0.0.1-SNAPSHOT.jar"]